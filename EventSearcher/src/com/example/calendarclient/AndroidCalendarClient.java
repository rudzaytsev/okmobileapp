package com.example.calendarclient;



import java.util.Calendar;

import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract.CalendarEntity;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Reminders;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.content.*;
import android.database.Cursor;



public class AndroidCalendarClient {
	
	
	private EventData eventData = new EventData();
	private ContentResolver resolver;
	
	public AndroidCalendarClient(){
		
	}
		
	public Calendar getEndEventDate(){
		return eventData.getDateEnd();
	}
	
	public void setTitle(String title){
		eventData.setTitle(title);
	}
	
	public String getTitle(){
		return eventData.getTitle();
	}
	
	public void setTimeZone(String timeZone){
		eventData.setEventTimeZone(timeZone);
	}
	
	public String getTimeZone(){
		return eventData.getEventTimeZone();
	}
	
	public void setDescription(String descript){
		eventData.setDescription(descript);
	}
	
	public String getDescription(){
		return eventData.getDescription();				
	}
	
	
	
	public ContentResolver getResolver() {
		return resolver;
	}

	/*
	* Developers MUST set up ContentResolver from the current Activity
	* to work with library (add events to calendar etc)
	* 
	* 
	*/
	public void setResolver(ContentResolver resolver) {
		this.resolver = resolver;
	}

	/*
	 * This method add a single event to calendar
	 * 
	 * @return long eventID of added event or -1 otherwise
	 * 
	 */
	
	public long addEvent(EventData eventData){
		
		Log.println(Log.INFO, "Try to get calendar values", "Start getting calendars ");  	

    	
    	String[] projection = new String[]{"_id", "calendar_displayName"};
    	Uri calendarsUri;
    	
    	// for sdk version > 8
		//calendarsUri = Uri.parse("content://com.android.calendar/calendars");
    	calendarsUri = Calendars.CONTENT_URI;
  			
    	Cursor managedCursor = resolver.query(calendarsUri, projection, null, null, null);	// all calendars
    	
    	int count = managedCursor.getCount();
    	
    	Log.println(Log.INFO, "Calendar Count Value", "Count Calendars value = " + count );
    	
    	while(managedCursor.moveToNext()){
    		
    		long calendID = 0;
    		
		    calendID = managedCursor.getLong(0);   // getting calendar Id
		    			
			// insert event

			ContentValues values = new ContentValues();
			values.put(Events.DTSTART, eventData.getDateStart().getTimeInMillis());
			values.put(Events.DTEND, eventData.getDateEnd().getTimeInMillis());
			values.put(Events.TITLE, eventData.getTitle());
			values.put(Events.DESCRIPTION, eventData.getDescription());			
			values.put(Events.CALENDAR_ID, calendID);
			values.put(Events.EVENT_TIMEZONE, eventData.getEventTimeZone());
			Uri uri = resolver.insert(Events.CONTENT_URI, values);
			
			long eventID = Long.parseLong(uri.getLastPathSegment());
			return eventID;
	    }
	
    	return -1;
	}
	
	public void deleteEvent(long eventId){
		
		
		Uri deleteUri = null;
		deleteUri = ContentUris.withAppendedId(Events.CONTENT_URI, eventId);
		int rows = resolver.delete(deleteUri, null, null);
		Log.println(Log.DEBUG,"Deleted event", "Rows deleted: " + rows);
		
	}
	
	public void addReminder(long eventId, int minutesBeforeEvent){
		
		ContentValues values = new ContentValues();
		values.put(Reminders.MINUTES, minutesBeforeEvent);
		values.put(Reminders.EVENT_ID, eventId);
		values.put(Reminders.METHOD, Reminders.METHOD_ALERT);
		resolver.insert(Reminders.CONTENT_URI, values);
	}
	
	
	
}
