package com.example.calendarclient;

import java.util.Calendar;

public class EventData {
	
	private Calendar dateStart;
	private Calendar dateEnd;
	private String title;
	private String description;
	private String eventTimeZone;
	
	
	public EventData(){
		
		dateStart = Calendar.getInstance();
		dateEnd = Calendar.getInstance();
	}

	public Calendar getDateStart() {
		return dateStart;
	}

	public void setDateStart(Calendar dateStart) {
		this.dateStart = dateStart;
	}

	public Calendar getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Calendar dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEventTimeZone() {
		return eventTimeZone;
	}

	public void setEventTimeZone(String eventTimeZone) {
		this.eventTimeZone = eventTimeZone;
	};
	
	/*
	* Method to set Start Event Date
	* Attention: month are counting from zero (January = 0, February = 1 etc)
	*
	*/    
	public void setStartEventDate(int year, int month, int day, int hour, int minute ){	
			
		dateStart.set(year, month, day, hour, minute );		
	}
		
	
	/*
	* Method to set End Event Date
	* Attention: month are counting from zero (January = 0, February = 1 etc)
	*
	*/
	public void setEndEventDate(int year, int month, int day, int hour, int minute ){
				
		dateEnd.set(year, month, day, hour, minute);		
	}
	
	
	
}