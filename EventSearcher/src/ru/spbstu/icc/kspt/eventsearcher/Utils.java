package ru.spbstu.icc.kspt.eventsearcher;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public final class Utils {
	
	private static int minutesBeforeRemindedEvent = 2;
	
	
	
	public static int getMinutesBeforeRemindedEvent() {
		return minutesBeforeRemindedEvent;
	}

	public static void setMinutesBeforeRemindedEvent(int minutesBeforeRemindedEvent) {
		Utils.minutesBeforeRemindedEvent = minutesBeforeRemindedEvent;
	}

	public static int getCurrentYear(){
		
		String date = getCurrentDate();
		int currentYear = Integer.parseInt( date.substring(6,10));
		return currentYear;
		
	}
	
	public static int getCurrentDay(){
		String date = getCurrentDate();
		int currentDay = Integer.parseInt( date.substring(0, 2) );
		return currentDay;		
	}
	
	/*
	 * 
	 * @return number of month, that starts from zero (Jan = 0, Feb = 1 etc) 
	 */
	public static int getCurrentMonth(){
		String date = getCurrentDate();
		int currentMonth = Integer.parseInt( date.substring(3, 5)) - 1;
		return currentMonth;		
	}
	
	/*
	 * 
	 * @return current Date String in format: dd.MM.yyyy HH:mm 
	 * 
	 */
	
	public static String getCurrentDate(){
		
		// getting current Date and current Year
		Calendar cal = new GregorianCalendar();				
	    String currentDate = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(cal.getTime());		
		return currentDate;
		
	}

}
