package ru.spbstu.icc.kspt.eventsearcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

class TemplateInfo{
	public ArrayList<TemplateElem> templates;
	private HashMap<Long, Integer> keys = new HashMap<Long, Integer>();
	
	public TemplateInfo(){
		templates = new ArrayList<TemplateElem>();
	}
	
	public void trim() {
		for(TemplateElem e : templates) {
			if(e.text.isEmpty()){
				keys.remove(e.id);
				templates.remove(e);
			}
		}
	}
	
	public void clearTasks() {
		for(TemplateElem e : templates) {
			e.isUpdated = false;
		}
	}
	
	public void update(Integer i, String text) {
		templates.get(i).text = text;
	}
	
	public void deleteAt(Integer i){// (T_T)
		int itr = 0;
		for(TemplateElem e : templates) {
			if(!e.text.isEmpty()){
				if(itr!=i) {
					++itr;
				} else {
					e.text = "";
					e.isUpdated = true;
					break;
				}
			}
		}
	}
	
	public String getText(Integer i) {
		return templates.get(i).text;
	}
	
	public boolean add(TemplateElem obj) {
		obj.isUpdated = true;
		if (templates.add(obj)) {
			keys.put(obj.id, templates.size()-1);
			return true;
		} else {
			return false;
		}
	}
	
	public ArrayList<String> toArrayList() {
		ArrayList<String> list = new ArrayList<String>();
		for(TemplateElem e : templates) {
			if(!e.text.isEmpty()){
				list.add(e.text);
			}
		}
		return list;
	}
	
	public int size() {
		return templates.size();
	}
	
	public Integer getPosById(Long id) {
		return keys.get(id);
	}
	
	public Long getIdByPos(Integer pos) {
		return templates.get(pos).id;
	}

	public void clear() {
		templates.clear();		
	}

	public boolean isEmpty() {
		return templates.isEmpty();
	}

	public Iterator iterator() {
		return templates.iterator();
	}

	public boolean remove(Object object) {
		return templates.remove(object);
	}
}