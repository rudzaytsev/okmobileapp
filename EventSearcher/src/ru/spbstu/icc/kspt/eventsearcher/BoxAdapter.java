package ru.spbstu.icc.kspt.eventsearcher;
import java.util.ArrayList;

import ru.spbstu.icc.kspt.eventsearcher.InfoElem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
public class BoxAdapter extends BaseAdapter {
Context ctx;
LayoutInflater lInflater;
ArrayList<InfoElem> objects;
ArrayList<InfoElem> data=new ArrayList<InfoElem>(); //data = countryList
private NameFilter filter;
BoxAdapter(Context context, ArrayList<InfoElem> list_object) {
	ctx = context;
	objects = list_object;
	lInflater = (LayoutInflater) ctx
	.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
}
// ���-�� ���������
@Override
public int getCount() {
	return objects.size();
}
// ������� �� �������
@Override
public Object getItem(int position) {
	return objects.get(position);
}
// id �� �������
@Override
public long getItemId(int position) {
	return position;
}
// ����� ������
@Override
public View getView(int position, View convertView, ViewGroup parent) {
	// ���������� ���������, �� �� ������������ view
	View view = convertView;
	if (view == null) {
		view = lInflater.inflate(R.layout.item, parent, false);
	}
	InfoElem p = getlist_class(position);
	// ��������� ������: ���, ���� � ��������
	((ImageView) view.findViewById(R.id.ivImage)).setImageBitmap(p.image);
	((TextView) view.findViewById(R.id.tvDescr)).setText(p.name);
	((TextView) view.findViewById(R.id.tvDate)).setText(p.date + "");
	CheckBox cbBuy = (CheckBox) view.findViewById(R.id.cbBox);
	// ����������� �������� ����������
	cbBuy.setOnCheckedChangeListener(myCheckChangList);
	// ����� �������
	cbBuy.setTag(position);
	// ��������� �������: ������� ��� ���
	cbBuy.setChecked(p.box);
	return view;
}

// ����/������� �� �������
InfoElem getlist_class(int position) {
	return ((InfoElem) getItem(position));
}

// ���������
ArrayList<InfoElem> getBox() {
	ArrayList<InfoElem> box = new ArrayList<InfoElem>();
	for (InfoElem p : objects) {
		// ���� �������:
		if (p.box)
			box.add(p);
	}
	return box;
}


// ���������� ��� ���������
OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			// ������ ����� 
			getlist_class((Integer) buttonView.getTag()).box = isChecked;
		}
	};


	
	
	// ������� ������������ �����, ���-�� ������
	public Filter getFilter() {
	    if (filter == null){
	     filter  = new NameFilter();
	    }
	    return filter;
	   }
	   private class NameFilter extends Filter
	   {

	    @Override
	    protected FilterResults performFiltering(CharSequence constraint) {

	     constraint = constraint.toString().toLowerCase();
	     FilterResults result = new FilterResults();
	     if(constraint != null && constraint.toString().length() > 0)
	     {
	     ArrayList<InfoElem> filteredItems = new ArrayList<InfoElem>();

	     for(int i = 0, l = objects.size(); i < l; i++)
	     {
	      InfoElem nameList = objects.get(i);
	      if(nameList.toString().toLowerCase().contains(constraint))
	       filteredItems.add(nameList);
	     }
	     result.count = filteredItems.size();
	     result.values = filteredItems;
	     }
	     else
	     {
	      synchronized(this)
	      {
	       result.values = objects;
	       result.count = objects.size();
	      }
	     }
	     return result;
	    }

	    @SuppressWarnings("unchecked")
		protected void publishResults(CharSequence constraint,
	      FilterResults results) {

	     data = (ArrayList<InfoElem>)results.values;
	     notifyDataSetChanged();	     
	    }
	   }
}
