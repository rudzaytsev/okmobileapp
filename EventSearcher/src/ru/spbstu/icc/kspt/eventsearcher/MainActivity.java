package ru.spbstu.icc.kspt.eventsearcher;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/*
 * Activity ��� ������� ��������� � �������
 */
public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.deleteDatabase("event_searcher_db");
        Intent intent = new Intent(this, MainService.class);
        intent.putExtra("cmd", MainService.Command.LOGIN);
    	startService(intent);
    }

    @Override
    public void onClick(View view) {
    	
    }
    
   /* public void onClickStop(View view) {
    	stopService(new Intent(this, MainService.class));
    }*/
    
    public void onClickGetFriends(View view) {
    	Intent intent = new Intent(this, MainService.class);
    	intent.putExtra("cmd", MainService.Command.UPDATE_FRIENDSINFO);
    	startService(intent);
    }
    
    public void onClickListFriends(View view) {
    	Intent intent = new Intent(this, MainService.class);
    	intent.putExtra("cmd", MainService.Command.LIST_FRIENDS);
    	startService(intent);
    }
    
 
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    }
}