package ru.spbstu.icc.kspt.eventsearcher;

class TemplateElem {
	Long id = -1L;
	Boolean isUpdated = false;
	String text = "";
	
	public TemplateElem(Long id, String text) {
		this.id = id;
		this.text = text;
	}
}