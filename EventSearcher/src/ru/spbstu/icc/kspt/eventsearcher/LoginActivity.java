package ru.spbstu.icc.kspt.eventsearcher;

import org.jivesoftware.smack.XMPPException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkTokenRequestListener;
import ru.ok.android.sdk.util.OkScope;

/*
 * Based on OK samples (which was created by valery.ozhiganov on 26.12.13).
 */
public class
        LoginActivity extends Activity implements OkTokenRequestListener, View.OnClickListener {

	private OkProcessor ok;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ok = new OkProcessor(getApplicationContext());
        ok.mOdnoklassniki = Odnoklassniki.createInstance(getApplicationContext(),
									ConstantsApiOk.APP_ID,
									ConstantsApiOk.APP_SECRET_KEY,
									ConstantsApiOk.APP_PUBLIC_KEY);

        ok.mOdnoklassniki.setTokenRequestListener(this);
        
        
//���� ���� ���-������ �����������, ����� ���� ��� ����� � ������������ ����� � ������
        
        try {
			ok.messenger.setLoginPassword("577672668959", "IHa+31+!11");
			ok.messenger.login();
		} catch (XMPPException e) {
			e.printStackTrace();
		}
    }

    @Override
    public void onDestroy() {
    	ok.mOdnoklassniki.removeTokenRequestListener();
        super.onDestroy();
    }

    @Override
    public void onSuccess(String token) {
        Log.v("APIOK", "Your token: " + token);
        finish();
    }

    //if something goes wrong in authorization
    @Override
    public void onError() {
        Log.v("APIOK", "Error");
        Toast.makeText(this, "Something goes wrong", Toast.LENGTH_LONG).show();
    }

    //if something goes wrong in authorization
    @Override
    public void onCancel() {
        Log.v("APIOK", "Cancel");
        Toast.makeText(this, "Something goes wrong", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginButton:
                Log.v("APIOK", "Clicked");
                ok.mOdnoklassniki.requestAuthorization(this, false, OkScope.VALUABLE_ACCESS);
                break;
        }
    }
}
