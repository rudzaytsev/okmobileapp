package ru.spbstu.icc.kspt.eventsearcher;

import java.util.List;

import org.joda.time.DateTime;

class TaskInfo{
	Long taskId;
	Long userId;
	Long templateId;
	Long time;
	
	public TaskInfo(Long taskId, Long userId, Long templateId, Long time){
		this.taskId = taskId;
		this.userId = userId;
		this.templateId = templateId;
		this.time = time;
	}
	
	public static Integer getPosOfTemp(List<TaskInfo> list, Long i) {
		int ind=0;
		for(TaskInfo p : list){
			if(p.templateId == i) return ind;
			++ind;
		}
		return -1;
	}
	
	public int getHours(){
		return (new DateTime(time)).getHourOfDay();
	}
	
	public int getMinutes(){
		return (new DateTime(time)).getMinuteOfHour();
	}
	
	public void setHours(Integer h){
		time = (new DateTime(time)).withHourOfDay(h).getMillis();
	}
	
	public void setMinutes(Integer m){
		time = (new DateTime(time)).withMinuteOfHour(m).getMillis();
	}
}