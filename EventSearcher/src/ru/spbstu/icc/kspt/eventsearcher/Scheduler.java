package ru.spbstu.icc.kspt.eventsearcher;
/*
 * Class under construction. Not for using.
 */
import java.util.HashMap;
import java.util.LinkedList;

import org.joda.time.DateTime;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Scheduler extends Thread {
    public static final String LOG_TAG = "Scheduler";
	public static enum Period {
    	NEVER, ONCE, DAY, WEEK, MONTH, YEAR, MINUTE;
    }
	private DbProcessor dbp;
	private Context context;
	private HashMap<Long, DateTime> activeTasks = new HashMap<Long, DateTime>();
	
    Scheduler(Context context) {
        this.context = context;
    	dbp = new DbProcessor(new DbConnector(context));
    }
    
    public void finishTask(Long sid) {
    	activeTasks.remove(sid);
    	dbp.openDatabase();
		dbp.refreshTaskTime(sid);
		dbp.closeDatabase();
    }

    @Override
    public void run() {
    	Log.v(LOG_TAG, "Started");
    	
    	DateTime dtz = DateTime.now().plusMinutes(1);
    	dbp.openDatabase();
    	//dbp.addTask(MainService.Command.SEND_MESSAGE, Period.MINUTE, dtz, 60000, "558285324374@XMPP: scheduler on duty.");
    	Long zz = dbp.addTask(MainService.Command.SEND_MESSAGE_TEMPLATE, Period.MINUTE, dtz, 45000, 548197445363L, "2");
    	Log.v(LOG_TAG, "Star    " + zz.toString());
    	dbp.closeDatabase();
    	DateTime dt, curdt;
    	while(true){
        	LinkedList<Intent> tasks = new LinkedList<Intent>();
        	dbp.openDatabase();
        	FillData.fillTaskList(dbp.getTaskAtTime(DateTime.now()), activeTasks, tasks, new Intent(context, MainService.class));
        	Log.e(LOG_TAG, "Tasks: "+Integer.toString(tasks.size()));
        	while(!tasks.isEmpty()){
        		context.startService(tasks.pop());
        	}
        	dt = dbp.getNextTaskTime(activeTasks);
        	dbp.closeDatabase();
    		if(dt == null){
    			dt = DateTime.now().plusMinutes(1);
    			Log.w(LOG_TAG, "dt == null");
    		}
    		curdt = DateTime.now();
    		if(dt.isAfter(curdt)) {
		        try {
		            sleep(dt.getMillis() - curdt.getMillis());
		        } catch (InterruptedException e) {
		            e.printStackTrace();
		            return;
		        }
    		} else {
    			if(this.isInterrupted()) return;
    		}
    	}
    }
}