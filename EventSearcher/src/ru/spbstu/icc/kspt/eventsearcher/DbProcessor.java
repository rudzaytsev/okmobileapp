package ru.spbstu.icc.kspt.eventsearcher;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.joda.time.DateTime;

import ru.spbstu.icc.kspt.eventsearcher.Scheduler.Period;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DbProcessor {
	public static final String LOG_TAG = "DbProcessor";
	// Correct it for multiple user tasks
	private static final String GET_FRIENDS_AND_RULES = "select uid, first_name, last_name, birthday, avatar_link, avatar_image, sid, params, time " +
														"from friends left join schedule on uid = user_id " +
														"order by last_name;";
	private static final String GET_FRIEND_RULES = "select uid, first_name, last_name, birthday, avatar_link, avatar_image, sid, params, time " +
													"from friends left join schedule on uid = user_id " +
													"where uid = ? " +
													"order by last_name;";
	
	private static DbConnector mDbConnector;
	private static AtomicInteger mOpenCounter = new AtomicInteger();
	private static SQLiteDatabase mSQLiteDatabase;
	
	DbProcessor(DbConnector mDbConnector){
		if(DbProcessor.mDbConnector==null) DbProcessor.mDbConnector = mDbConnector;
	}
	
    public synchronized void openDatabase() {
    	mOpenCounter.incrementAndGet();
        mSQLiteDatabase = mDbConnector.getWritableDatabase();
    }

    public synchronized void closeDatabase() {
        if(mOpenCounter.decrementAndGet() == 0) {
        	mSQLiteDatabase.close();
        }
    }

	public Cursor getFriendsInfo(Long uid){
		Cursor cur;
		if (uid == null) {
			cur = mSQLiteDatabase.query("friends", null,
			        null, null, null, null, "last_name");
		} else {
			cur = mSQLiteDatabase.query("friends", null,
			        "uid = "+uid.toString(), null, null, null, "last_name");
		}
		return cur;
	}
	
	public Cursor getFriendsTasksInfo(Long uid) {
		Cursor cur;
		if (uid == null) {
			cur = mSQLiteDatabase.rawQuery(GET_FRIENDS_AND_RULES, null);
		} else {
			cur = mSQLiteDatabase.rawQuery(GET_FRIEND_RULES, new String[]{uid.toString()});
		}
		return cur;
	}
		
	public String getMessageTemplate(Long tid) {
		Cursor cur = mSQLiteDatabase.query("message_templates", new String[]{"template"},
		        "tid = ?", new String[]{tid.toString()}, null, null, null);
		if(cur.moveToFirst()){
			return cur.getString(0);
		}else{
			return "Grac!";
		}
	}

	public void updateTemplate(Long tid, String template){
		ContentValues cv = new ContentValues();
		cv.put("template", template);
		mSQLiteDatabase.update("message_templates", cv, "tid = ?", new String[]{tid.toString()});
	}
	
	public void deleteTemplate(Long tid){
		mSQLiteDatabase.delete("message_templates", "tid = ?", new String[]{tid.toString()});
	}
	
	public Long addTemplate(String template){
		ContentValues cv = new ContentValues();
		cv.put("template", template);
		return mSQLiteDatabase.insert("message_templates", null, cv);
	}
	
	public Long addTask(MainService.Command cmd, Scheduler.Period period, DateTime firstTime, Integer timeout, Long uid, String params){
		ContentValues cv = new ContentValues();
    	cv.put("period", period.ordinal());
    	cv.put("task_id", cmd.ordinal());
    	cv.put("timeout", timeout);
    	cv.put("user_id", uid.toString());
    	if(params != null)cv.put("params", params);
		cv.put("time", firstTime.withSecondOfMinute(0).withMillisOfSecond(0).getMillis());
		return mSQLiteDatabase.insert("schedule", null, cv);
	}
	
	public void updateTask(Long sid, MainService.Command cmd, Scheduler.Period period, DateTime firstTime, Integer timeout, Long uid, String params){
		ContentValues cv = new ContentValues();
		if(period != null)cv.put("period", period.ordinal());
    	if(cmd != null)cv.put("task_id", cmd.ordinal());
    	if(timeout != null)cv.put("timeout", timeout);
    	if(uid != null)cv.put("user_id", uid.toString());
    	if(params != null)cv.put("params", params);
    	if(firstTime != null)cv.put("time", firstTime.withSecondOfMinute(0).withMillisOfSecond(0).getMillis());
		mSQLiteDatabase.update("schedule", cv, "sid = ?", new String[]{sid.toString()});
	}
	
	public void deleteTask(Long sid){
		mSQLiteDatabase.delete("schedule", "sid = ?", new String[]{sid.toString()});
	}
	
	public DateTime getNextTaskTime(HashMap<Long, DateTime> activeTasks){
		DateTime nextTimeout = new DateTime(Long.MAX_VALUE);
		DateTime curTimeout = new DateTime();
		StringBuilder builder = new StringBuilder();
		for(int i=0; i<activeTasks.size(); ++i){
			curTimeout = activeTasks.get(i);
			if(curTimeout!= null && curTimeout.isAfterNow()) {
				builder.append(",'").append(activeTasks.get(i)).append("'");
				if(curTimeout.isBefore(nextTimeout)) {
					nextTimeout = curTimeout;
				}
			}
		}
		Cursor cur = mSQLiteDatabase.query("schedule", new String[]{"time"},
		        "period <> ?" + (builder.length() > 0 ? " and sid not in (" + builder.substring(1) +")":""),
				new String[]{Integer.toString(Period.NEVER.ordinal())},
                null, null, "time", "1");
		if(cur.moveToFirst()){
			Log.w(LOG_TAG, (new DateTime(cur.getLong(0))).toString() +(" period <> ?" + (builder.length() > 0 ? " and sid not in (" + builder.substring(1) +")":"zzz")));
			curTimeout = new DateTime(cur.getLong(0));
			if(curTimeout.isBefore(nextTimeout)) {
				nextTimeout = curTimeout;
			}
		}
		return nextTimeout;
	}
	
	public Cursor getTemplate(Long tid){
		Cursor cur;
		if (tid == null) {
			cur = mSQLiteDatabase.query("message_templates", null,
			        null, null, null, null, null);
		} else {
			cur = mSQLiteDatabase.query("message_templates", null,
			        "uid = "+tid.toString(), null, null, null, null);
		}
		return cur;
	}
	
	public Cursor getTaskAtTime(DateTime dt){
		Cursor cur = mSQLiteDatabase.query("schedule", new String[]{"sid", "time", "task_id", "user_id", "period", "params", "timeout"},
		        "time <= ? AND period <> ?",
				new String[]{Long.toString(dt.getMillis()), Integer.toString(Period.NEVER.ordinal())},
                null, null, null);
		return cur;
	}
	
	public void refreshTaskTime(Long sid){
		Cursor cur = mSQLiteDatabase.query("schedule", new String[]{"time", "period"},
		        "sid = ?", new String[]{sid.toString()},
                null, null, null);
		if(cur.moveToFirst()){
			DateTime curdt = new DateTime();
			DateTime dt = new DateTime(cur.getLong(cur.getColumnIndex("time")));
			ContentValues cv = new ContentValues();
			Scheduler.Period period = Scheduler.Period.values()[cur.getInt(cur.getColumnIndex("period"))];
			if(period == Scheduler.Period.ONCE){
				cv.put("period", Scheduler.Period.NEVER.ordinal());
				mSQLiteDatabase.update("schedule", cv, "sid = ?", new String[]{sid.toString()});
			}else{
				switch(period){
					case DAY:
						while(dt.isBefore(curdt)){dt = dt.plusDays(1);};
						break;
					case WEEK:
						while(dt.isBefore(curdt)){dt = dt.plusWeeks(1);};
						break;
					case MONTH:
						while(dt.isBefore(curdt)){dt = dt.plusMonths(1);};
						break;
					case YEAR:
						while(dt.isBefore(curdt)){dt = dt.plusYears(1);};
						break;
					case MINUTE:
						while(dt.isBefore(curdt)){dt = dt.plusMinutes(1);};
						break;
					default:
						return;
				}
				cv.put("time", dt.getMillis());
				mSQLiteDatabase.update("schedule", cv, "sid = ?", new String[]{sid.toString()});
			}
		}
	}
	
	public Cursor getAvatar(Long uid){
		Cursor cur = mSQLiteDatabase.query("friends", new String[]{"uid", "avatar_link"}, uid == null ? null : "uid = "+ uid.toString(), null, null, null, "uid");
		return cur;
	}
	
	public void setAvatar(Long uid, byte[] imager){
		ContentValues cv = new ContentValues();
		cv.put("avatar_image", imager);
		mSQLiteDatabase.update("friends", cv, "uid = ?", new String[]{uid.toString()});
	}
	
	public void updateTemplatesFromList(TemplateInfo templates) {
		for(TemplateElem p : templates.templates) {
			Log.v(LOG_TAG, p.text);
			if(p.isUpdated) {
				if(p.text.isEmpty()) {
					deleteTemplate(p.id);
				} else {
					if(p.id == null) {
						addTemplate(p.text);
					} else {
						updateTemplate(p.id, p.text);
					}
				}
			}
		}
	}
	
	public void updateUser(Long uid, String firstName, String lastName, String birthday, String avatarLink){
		ContentValues cv = new ContentValues();
		if(firstName != null) cv.put("first_name", firstName);
    	if(lastName != null) cv.put("last_name", lastName);
    	if(birthday != null) cv.put("birthday", birthday);
		if(avatarLink != null) cv.put("avatar_link", avatarLink);
		mSQLiteDatabase.update("friends", cv, "uid = ?", new String[]{uid.toString()});
	}
	
	public void addUser(Long uid, String firstName, String lastName, String birthday, String avatarLink){
		ContentValues cv = new ContentValues();
		cv.put("uid", uid);
    	cv.put("first_name", firstName);
    	cv.put("last_name", lastName);
    	cv.put("birthday", birthday);
		cv.put("avatar_link", avatarLink);
		mSQLiteDatabase.insert("friends", null, cv);
	}
}
