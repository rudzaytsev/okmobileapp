package ru.spbstu.icc.kspt.eventsearcher;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.util.Log;


public class ImageLoader {
	public static final String LOG_TAG = "ImageLoader";
	public static byte[] getByteArrayFromURL(String link) {
	    try {
	        URL url = new URL(link);
	        HttpURLConnection connection = (HttpURLConnection) url
	                .openConnection();
	        connection.setDoInput(true);
	        connection.connect();
	        InputStream input = connection.getInputStream();
	        byte[] buffer = new byte[70000];
	        int bytesRead;
	        ByteArrayOutputStream output = new ByteArrayOutputStream();
	        while ((bytesRead = input.read(buffer)) != -1)
	        {
	            output.write(buffer, 0, bytesRead);
	        }
	        return output.toByteArray();
	    } catch (IOException e) {
	        e.printStackTrace();
	        Log.e(LOG_TAG, e.getMessage().toString());
	        return null;
	    }
	}
}

