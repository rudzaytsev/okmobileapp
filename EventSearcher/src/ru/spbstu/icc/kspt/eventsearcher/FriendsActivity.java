package ru.spbstu.icc.kspt.eventsearcher;

import java.util.ArrayList;

import org.joda.time.DateTime;

import ru.spbstu.icc.kspt.eventsearcher.Scheduler.Period;

import com.example.calendarclient.AndroidCalendarClient;
import com.example.calendarclient.EventData;

import android.R.menu;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.CheckBox;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;



public class FriendsActivity extends Activity  {	
	private TabHost tabHost;
	public static final String LOG_TAG = "FriendsActivity";
	private static final int IDM_CHK_BOX=1001; //check box
	private static final int IDM_DLT=1002; //delete database
	private static final int IDM_FIND=1003; //Search
	private static final int IDM_Time=1004; //Settings
	private static final int IDM_Template=1005; //Settings
	
	ArrayList<InfoElem> friendList = new ArrayList<InfoElem>(); // ������ �� �� ��
	ArrayList<InfoElem> eventList = new ArrayList<InfoElem>(); // ������ ������� �������� (������ �������)
	ArrayList<TaskInfo> taskInfo = new ArrayList<TaskInfo>();
	BoxAdapter boxAdapterSync;
	BoxAdapter boxAdapterCongr;
	
	// ������ �����
	int DIALOG_TIME = 1;
	int myHour = 12;
	int myMinute = 00;
	boolean check_box=false;
	EditText inputSearch;
	EditText inputSearch1;
	
	Long selectedTemplateId = 1L;
	/** Called when the activity is first created. */
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		
		setContentView(R.layout.activity_list);
		
		tabHost = (TabHost)findViewById(R.id.tabhost);
        tabHost.setup();

        
        TabHost.TabSpec friend_tab = tabHost.newTabSpec("������");
        friend_tab.setIndicator(createTabView(tabHost.getContext(), "������"));
        friend_tab.setContent(R.id.friends_layout);
        tabHost.addTab(friend_tab);
        
        TabHost.TabSpec events_tab = tabHost.newTabSpec("������������");
        events_tab.setIndicator(createTabView(tabHost.getContext(), "������������"));
        events_tab.setContent(R.id.congratulations_layout);
        tabHost.addTab(events_tab);
        
        tabHost.setCurrentTab(0);       
        
		// ������� �������
        
        new ShowFriends().execute(getBaseContext()); // ������� "������"
        new Show�ongratulations().execute(getBaseContext()); // ������� "������������"

       
	}
	
	
	private static View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);
		return view;
    }
	
	@Override
    public void onActivityResult(int requestCode,int resultCode,Intent data)
    {
     //super.onActivityResult(requestCode, resultCode, data);
     selectedTemplateId = data.getLongExtra("selectedTemplateId", 1L);
     InfoElem.setBox(eventList, taskInfo, selectedTemplateId);
     boxAdapterCongr.notifyDataSetChanged();
     int i = TaskInfo.getPosOfTemp(taskInfo, selectedTemplateId);
     if(i>0) {
    	 myHour = taskInfo.get(i).getHours();
    	 myMinute = taskInfo.get(i).getMinutes();
     }
    }
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		/*menu.add(Menu.NONE, IDM_CHK_BOX, 1, "Check all")
		.setIcon(R.drawable.checkbox_checked)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);*/
		menu.add(Menu.NONE, IDM_DLT, 2, "�������� ���������")
		.setIcon(android.R.drawable.ic_menu_delete)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		menu.add(Menu.NONE, IDM_Time, 3, "����� �����������")
		.setIcon(android.R.drawable.ic_menu_more)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		menu.add(Menu.NONE, IDM_Template, 4, "������ ������������")
		.setIcon(android.R.drawable.ic_menu_more)
		.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return super.onCreateOptionsMenu(menu);
    }
	
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("MENU", "Cliced MenuItem is " + item.getTitle());
       // CheckBox cbox = (CheckBox)findViewById(R.id.cbBox);
        switch (item.getItemId())
        {
            case  IDM_DLT:
            	//this.deleteDatabase("event_searcher_db");
            	recreate(); // ������������ ����
            	//������� ��� ������� �� ���������, ������� �������������� ������������

            	break;
            case  IDM_CHK_BOX: //�������� ���, ����� ��� (�� ��������, ���� �������� �� ���� ��������� ������)
            	if (check_box){
            		// �������� �� ������, ������� ��������
            		for (InfoElem p : boxAdapterSync.getBox()) {
            			CheckBox cbox = (CheckBox)findViewById(R.id.cbBox);
            			if (p.box) {
            				cbox.setChecked(false);
            			}
            			else 
            				cbox.setChecked(false);
            		}
            		check_box=false;
            		item.setTitle("Check all");
            		item.setIcon(R.drawable.checkbox_yes);
            	}
            	else{
            		// �������� �� ������, ������� ��������
            		for (InfoElem p : boxAdapterSync.getBox()) {
            			CheckBox cbox = (CheckBox)findViewById(R.id.cbBox);
            			if (p.box) {
            				cbox.setChecked(true);
            			}
            			else 
            				cbox.setChecked(true);
            		}
            		check_box=true;
            		item.setTitle("Uncheck all");
            		item.setIcon(R.drawable.checkbox_unchecked);
            	}
            		
            	//recreate();            	
            	break;
            case IDM_Template:
            	//Intent intent = new Intent(this, MainService.class);
            	Intent intent = new Intent(this, TemplatesActivity.class);
            	//intent.putExtra("cmd", MainService.Command.LIST_TEMPLATES);
            	intent.putExtra("selTid", selectedTemplateId);
            	//startService(intent);
            	startActivityForResult(intent, 1);
            	break;
            case IDM_Time:
            	showDialog(DIALOG_TIME);
            	break;
            
            default:
                 return false;
        }
		return true;
    }

 

    // ������� ���������� � ��������� ������� (�������������)
	public void showResult(View v) {
		String result = "�������:";
		for (InfoElem p : boxAdapterSync.getBox()) {
			if (p.box)
				result += "\n" + p.name;
			}
		Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
	}
	
	// ������� ���������� � ��������� ������� (������������)
	public void showResult1(View v) {
		String result = "�������:";
		for (InfoElem q : boxAdapterCongr.getBox()) {
			if (q.box)
				result += "\n" + q.name;
			}
		Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
	}

	public void onClickSynchr(View v) {
		//showResult(v);
		//showDialog(DIALOG_TIME);
		AndroidCalendarClient client = new AndroidCalendarClient();
		client.setResolver(getContentResolver());
		
		
		ArrayList<Long> list = new ArrayList<Long>();
		// �������� �� ������, ������� ��������
		for (InfoElem p : boxAdapterSync.getBox()) {
			
			EventData data = new EventData();
			if (p.box) {
				// ���������
				this.setEvents(client, data, p.date, p.name, list);				
				this.setReminders(client, list);
			}
		}
	}
	
	private void setEvents(AndroidCalendarClient client,EventData eventData, String date, String friendName, ArrayList<Long> eventIds  ){
		int month = 0;
		int day = 0;
		if(date.length() == 10){	// ��� �������� ����� ������
			// �������� ������ � ����!
			month = Integer.parseInt(date.substring(5, 7)) - 1;
			day = Integer.parseInt(date.substring(8));			
		}
		else {	// ��� �� ������
			// �������� ������ � ����!
			month = Integer.parseInt(date.substring(0, 2)) - 1;
			day = Integer.parseInt(date.substring(3));
		}
		
		// ��������� � ���������
		eventData.setStartEventDate(Utils.getCurrentYear(), month, day, myHour, myMinute); // ? ����� �� ����������� ����� ���. �������?
		// �������� �������, ������ ��� � ��������� ��������� � ����
		// �� ���� ����� ��� ������, ����� 1-1=0
		eventData.setEndEventDate(Utils.getCurrentYear(), month, day, 23, 59);
		eventData.setTitle("#OK ���� ��������");
		eventData.setDescription("������� ���� �������� �������� " + friendName);
		eventData.setEventTimeZone("Russia/Moscow");//���� �� ���? ��� ������ ������ �� ��� ����, ����� ����� �������
		long eventId = client.addEvent(eventData);
		//��������� ������ � ������ �������
		eventIds.add(eventId);
		
	}
	
	private void setReminders(AndroidCalendarClient client ,ArrayList<Long> eventIds){
		for(Long eventId : eventIds){
			client.addReminder(eventId, Utils.getMinutesBeforeRemindedEvent());
		}
	}
	
	public void onClickSynchr1(View v) {
		showResult1(v);
		DbProcessor dbp = new DbProcessor(new DbConnector(getApplicationContext()));
		dbp.openDatabase();
		InfoElem p;
		for (int i =0; i < boxAdapterCongr.objects.size(); ++i) {
			p = boxAdapterCongr.objects.get(i);
			Log.v(LOG_TAG, "Cur: "+p.name);
			if (p.box) {
				DateTime dt = new DateTime(taskInfo.get(i).time).withHourOfDay(myHour).withMinuteOfHour(myMinute);
				Log.v(LOG_TAG, "taskInfo "+taskInfo.get(i).time.toString());
				Log.v(LOG_TAG, "DateTime "+dt.toString());
				while(dt.isBeforeNow()) {
					dt = dt.plusYears(1);
				}
				Log.v(LOG_TAG, "DateTime "+dt.toString());
				taskInfo.get(i).templateId = selectedTemplateId;
				if(taskInfo.get(i).taskId>=0) {
					Log.v(LOG_TAG, "Upd: "+p.name + " " + taskInfo.get(i).taskId.toString() + " " + taskInfo.get(i).templateId.toString());
					dbp.updateTask(taskInfo.get(i).taskId, null, null, dt, null, null, taskInfo.get(i).templateId.toString());
					//dbp.updateTask(taskInfo.get(i).taskId, null, null, null, null, null, taskInfo.get(i).templateId.toString());
				} else {
					Log.v(LOG_TAG, "Add: "+p.name + " " + taskInfo.get(i).userId.toString());
					taskInfo.get(i).taskId = dbp.addTask(MainService.Command.SEND_MESSAGE_TEMPLATE, Period.YEAR, dt, 45000, taskInfo.get(i).userId, taskInfo.get(i).templateId.toString());
				}
				taskInfo.get(i).setHours(myHour);
				taskInfo.get(i).setMinutes(myMinute);
			} else if(taskInfo.get(i).taskId>=0) {
				if(taskInfo.get(i).templateId == selectedTemplateId) taskInfo.get(i).templateId = -1L;
				Log.v(LOG_TAG, "Del: "+p.name);
				//dbp.deleteTask(taskInfo.get(i).taskId);
			}
		}
		dbp.closeDatabase();
	}
	
	private class ShowFriends extends AsyncTask<Context, Void, String> {
		private Context context;
        @Override
        protected String doInBackground(Context... params) {
        	Log.v(LOG_TAG, "Started");
        	context = params[0];
        	DbProcessor dbp = new DbProcessor(new DbConnector(context));
        	dbp.openDatabase();
    		FillData.fillFriendList(dbp.getFriendsInfo(null), friendList, null);
    		dbp.closeDatabase();
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
        	boxAdapterSync = new BoxAdapter(context, friendList);
    		ListView lvMain_us = (ListView) findViewById(R.id.user_list);
    		//inputSearch = (EditText) findViewById(R.id.searchtext);
    		lvMain_us.setAdapter(boxAdapterSync);
    		/*inputSearch.addTextChangedListener(new TextWatcher() {

    			public void afterTextChanged(Editable arg0) {
    			}

    			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
    			}

    			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
    				// �����, ���� �������� ����� �� ��������
    				boxAdapter.getFilter().filter(cs);
    			}
    		});*/
    		
        }
	}
	
	
	private class Show�ongratulations extends AsyncTask<Context, Void, String> {
		private Context context1;
        @Override
        protected String doInBackground(Context... params) {
        	Log.v(LOG_TAG, "Started");
        	context1 = params[0];
        	DbProcessor dbp = new DbProcessor(new DbConnector(context1));
        	dbp.openDatabase();
        	FillData.fillFriendList(dbp.getFriendsTasksInfo(null), eventList, taskInfo);
    		dbp.closeDatabase();
    		
    		InfoElem.setBox(eventList, taskInfo, selectedTemplateId);
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
    		boxAdapterCongr = new BoxAdapter(context1, eventList);
    		ListView lvMain_ev = (ListView) findViewById(R.id.congratulations_list);
    		//������� ������� �����
    		/*inputSearch1 = (EditText) findViewById(R.id.searchtext1);
    		inputSearch1.addTextChangedListener(new TextWatcher() {

    			public void afterTextChanged(Editable arg0) {
    			}

    			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
    				// TODO Auto-generated method stub
    			}

    			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

    				// �����, ���� �������� ����� �� ��������
    				boxAdapterCongr.getFilter().filter(cs);
    			}
    		});*/
    		lvMain_ev.setAdapter(boxAdapterCongr);
    		int i = TaskInfo.getPosOfTemp(taskInfo, selectedTemplateId);
    		if(i>0) {
    	    	 myHour = taskInfo.get(i).getHours();
    	    	 myMinute = taskInfo.get(i).getMinutes();
    	     }
        }
	}
	

	
	 // ������ ������� �������
	protected Dialog onCreateDialog(int id) {
	      if (id == DIALOG_TIME) {
	        TimePickerDialog tpd = new TimePickerDialog(this, myCallBack, myHour, myMinute, true);
	        tpd.setTitle("�������� ����� �����������");
	        return tpd;
	      }
	      return super.onCreateDialog(id);
	    }
	OnTimeSetListener myCallBack = new OnTimeSetListener() {
	    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	      myHour = hourOfDay; // ���������� ��������
	      myMinute = minute; 
	    }
	  };
	
	  @Override
	    public void onDestroy() {
	    	super.onDestroy();
	    }
    
}