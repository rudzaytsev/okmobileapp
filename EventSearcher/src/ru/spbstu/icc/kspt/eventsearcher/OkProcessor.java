package ru.spbstu.icc.kspt.eventsearcher;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.SSLContext;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackAndroid;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.ok.android.sdk.Odnoklassniki;
import android.content.Context;

import android.os.AsyncTask;
import android.util.Log;



/*
 * ����� ������� ��� ������� � API, ������ ������� ����������� ��� AsyncTask
 * ������� ����� �� "��������" ��-�� ������� ��������
 */
class OkProcessor {
    public static final String LOG_TAG = "OkProcessor";
    public static Odnoklassniki mOdnoklassniki;
    private static Context mContext;
    public static Messenger messenger;
    private static SmackAndroid smackAndroid;
    
    public OkProcessor(Context context) {
    	if(mContext == null) mContext = context;
    	if(smackAndroid == null) smackAndroid = SmackAndroid.init(context);
    	if(messenger == null) messenger = new Messenger();
    }
    
    public void update(Odnoklassniki obj) {
        mOdnoklassniki = obj;
        Log.v(LOG_TAG, mOdnoklassniki.getCurrentAccessToken());
    }

    public void getFriendsInfo() {
        if(mOdnoklassniki!=null){
        	new LoadFriends().execute();
        }else{
        	Log.w(LOG_TAG, "getFriendsInfo() : mOdnoklassniki==null");
        }
    }
    
    public void sendMessage(String uid, String message) {
    	messenger.sendMessage(uid, message);
    }
      
    private class LoadFriends extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                return mOdnoklassniki.request("friends.get", null, "get");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.v(LOG_TAG, "Response on friends.get: " + result);
                // ��-�� ����������� API �� �� ����� ����� ��������� ���������� ����� ��� � 100 �������������,
                // � ������ ����� ���� �� 10000
                ArrayList<String> friendsId = jsonArrayToStringArray(result, ConstantsApiOk.UsersGetInfoMaxUids);
                for (int i = 0; i < friendsId.size(); i++) {
                    new LoadFriendsInfo().execute(friendsId.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    
        // ����������� ���������� �� ������� ������� � ������ �� ����� �������������
        private ArrayList<String> jsonArrayToStringArray(String str, int maxElemPerString) throws JSONException {
            ArrayList<String> result = new ArrayList<String>();
            JSONArray array = new JSONArray(str);
            StringBuilder builder = new StringBuilder();
            for (int iInStr = 0, iInArr = 0; iInArr < array.length(); iInArr++){
                if (iInStr == maxElemPerString) {
                    result.add(builder.substring(1));
                    builder = new StringBuilder();
                    iInStr = 0;
                }
                String element = array.getString(iInArr);
                builder.append(',').append(element);
                iInStr++;
            }
            result.add(builder.substring(1));
            return result;
        }
    }
    
   
    private class LoadFriendsInfo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String friendsId = params[0];
            Map<String, String> requestParams = new HashMap<String, String>();
            requestParams.put("uids", friendsId);
            requestParams.put("fields", "last_name, first_name, birthday, pic50x50");
            try {
                return mOdnoklassniki.request("users.getInfo", requestParams, "get");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    
        @Override
        protected void onPostExecute(String result) {
            Log.v("APIOK", "Response on users.getInfo: " + result);
            DbProcessor dbp = new DbProcessor(new DbConnector(mContext));
            dbp.openDatabase();
            Map<Long, String> linkMapInOk = new HashMap<Long, String>();
            Map<Long, String> linkMapInDb = new HashMap<Long, String>();
            try {
				JSONArray array = new JSONArray(result);
				JSONObject obj;
				FillData.fillAvatar(dbp.getAvatar(null), linkMapInDb);
				
				String linkInDb, linkInOk;
				for (int i = 0; i < array.length(); i++) {
					obj = array.getJSONObject(i);
		        	linkInDb = linkMapInDb.get(obj.getLong("uid"));
		        	linkInOk = obj.getString("pic50x50").split("\\&", 2)[0];
		        	if(linkInDb != null){
		        		if(!linkInDb.equals(linkInOk)){
		        			linkMapInOk.put(obj.getLong("uid"), obj.getString("pic50x50"));
		        			dbp.updateUser(obj.getLong("uid"), obj.getString("first_name"), obj.getString("last_name"),
		        												obj.getString("birthday"), linkInOk);
		        		}
		        		dbp.updateUser(obj.getLong("uid"), obj.getString("first_name"), obj.getString("last_name"),
													obj.getString("birthday"), null);
		        	} else {
		        		dbp.addUser(obj.getLong("uid"), obj.getString("first_name"), obj.getString("last_name"),
												obj.getString("birthday"), linkInOk);
		        		linkMapInOk.put(obj.getLong("uid"), obj.getString("pic50x50"));
		        	}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
            dbp.closeDatabase();
            new LoadAvatars().execute(linkMapInOk);
        }
    }
    
    private class LoadAvatars extends AsyncTask<Map<Long, String>, Void, String> {
        @Override
        protected String doInBackground(Map<Long, String>... params) {
        	Map<Long, String> links = params[0];
            DbProcessor dbp = new DbProcessor(new DbConnector(mContext));
            dbp.openDatabase();
            try {
            	for( Entry<Long, String> entry : links.entrySet() ){
                    dbp.setAvatar(entry.getKey(), ImageLoader.getByteArrayFromURL(entry.getValue()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            dbp.closeDatabase();
            return null;
        }
    }
    
    class Messenger implements MessageListener{
    	XMPPTCPConnection  connection;
    	String login;
    	String password;
    	
        public void login() throws XMPPException
        {
	        ConnectionConfiguration config = new ConnectionConfiguration(ConstantsApiOk.XMPP_SERVER, 
	        																ConstantsApiOk.XMPP_PORT);
	        SSLContext sc;
	        SASLAuthentication.supportSASLMechanism("PLAIN");
			try {
				sc = SSLContext.getInstance("TLS");
				config.setCustomSSLContext(sc);
				config.setReconnectionAllowed(true);
				config.setSecurityMode(SecurityMode.disabled);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
	        connection = new XMPPTCPConnection(config);
	        new ConnectMessenger().execute(connection);
        }

        private class AsyncSendMessage extends AsyncTask<Message, Void, Void> {
	        @Override
	        protected Void doInBackground(Message... params) {
	            try {
	            	connection.sendPacket(params[0]);
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return null;
	        }
        }
        
        public void sendMessage(String uid, String message){
        	Message msg = new Message(uid+"@odnoklassniki.ru", Message.Type.chat);
			msg.setBody(message);
			if (connection != null) {
				new AsyncSendMessage().execute(msg);
			}
        }
        
		@Override
		public void processMessage(Chat arg0, Message arg1) {
			Log.v(LOG_TAG, "O_O");
		}
		
		public void disconnect() {
			try {
				connection.disconnect();
				smackAndroid.onDestroy();
			} catch (NotConnectedException e) {
				e.printStackTrace();
			}
		}
		
		public void setLoginPassword(String login, String password) {
			this.login = login;
			this.password = password;
		}
		
	    private class ConnectMessenger extends AsyncTask<XMPPTCPConnection, Void, Void> {
	        @Override
	        protected Void doInBackground(XMPPTCPConnection... params) {
				try {
					params[0].connect();
					params[0].login(login, password);
				} catch (Exception e) {
					e.printStackTrace();
				}
	            return null;
	        }
	    }
    }


	public void disconnet() {
		messenger.disconnect();
	}
}