package ru.spbstu.icc.kspt.eventsearcher;

import org.joda.time.DateTime;

import ru.spbstu.icc.kspt.eventsearcher.Scheduler.Period;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbConnector extends SQLiteOpenHelper {
	public static final String LOG_TAG = "DbConnector";

	private static final int DB_VERSION = 1;
	private static final String DB_NAME = "event_searcher_db";

	private static final String CREATE_TABLE_FRIENDS 
		= "create table friends ( uid integer primary key" +
									", first_name text" +
									", last_name text" +
									", birthday text" +
									", avatar_link text" +
									", avatar_image blob" +
									"); ";
	private static final String CREATE_TABLE_SHEDULE 
		= "create table schedule ( sid integer primary key autoincrement" +
									", time integer" +
									", period integer" +
									", timeout integer" +
									", task_id integer" +
									", user_id integer" +
									", params text" +
									");";
	
	private static final String CREATE_MESSAGE_TEMPLATES 
	= "create table message_templates ( tid integer primary key autoincrement" +
								", template text" +
								");";
	
	private static final String CREATE_DEFAULT_SCHEDULE 
    = "INSERT INTO schedule (time, period, timeout, task_id) " +
      "VALUES (" + DateTime.now().plusMinutes(1).withSecondOfMinute(0).withMillisOfSecond(0).getMillis() + 
              ", " + Period.DAY.ordinal() +
              ", 300000" +
              ", " + MainService.Command.UPDATE_FRIENDSINFO.ordinal() +
              ");";
	
	private static final String CREATE_DEFAULT_MSG_TEMPLATE 
    = "INSERT INTO message_templates (template) " +
      "VALUES ('��������� $first_name $last_name, � ����������� �������� ���, ��� ��� ��������� ��������� ������� � ���������� ��������� ����������.');";
	private static final String CREATE_DEFAULT_MSG_TEMPLATE1 
    = "INSERT INTO message_templates (template) " +
      "VALUES ('� ���� ��������, $first_name! ���� ���� � LLaP!');";
	private static final String CREATE_DEFAULT_MSG_TEMPLATE2 
    = "INSERT INTO message_templates (template) " +
      "VALUES ('�� ����� ��������, � ������ ����� �� ��� ���������� �������, � �� ����� ���������� ���� � ���� �������� ����������!');";
	private static final String CREATE_DEFAULT_MSG_TEMPLATE3 
    = "INSERT INTO message_templates (template) " +
      "VALUES ('� ���� ��������, $first_name! May the force be with you!');";

	public DbConnector(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	    db.execSQL(CREATE_TABLE_FRIENDS);
	    db.execSQL(CREATE_TABLE_SHEDULE);
	    db.execSQL(CREATE_DEFAULT_SCHEDULE);
	    db.execSQL(CREATE_MESSAGE_TEMPLATES);
	    db.execSQL(CREATE_DEFAULT_MSG_TEMPLATE2);
	    db.execSQL(CREATE_DEFAULT_MSG_TEMPLATE);
	    db.execSQL(CREATE_DEFAULT_MSG_TEMPLATE1);
	    db.execSQL(CREATE_DEFAULT_MSG_TEMPLATE3);
	}

	@Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}