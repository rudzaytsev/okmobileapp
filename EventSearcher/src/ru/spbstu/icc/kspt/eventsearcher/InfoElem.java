package ru.spbstu.icc.kspt.eventsearcher;

import java.util.List;

import android.graphics.Bitmap;
import android.util.Log;

public class InfoElem {
	private static String LOG_TAG = "InfoElem";
	
	String name;
	String date;
	Bitmap image;
	boolean box;
	InfoElem(String name_item, String date, Bitmap image, boolean box) {
		this.name = name_item;
		this.date = date;
		this.image = image;
		this.box = box;
	}
	
	static public void setBox(List<InfoElem> events, List<TaskInfo> tasks, Long selId){
		//Log.v(LOG_TAG, "selId: "+ selId.toString());
		for(int i=0; i<events.size(); ++i) {
			//Log.v(LOG_TAG, "Before: "+ Boolean.toString(events.get(i).box));
			if(tasks.get(i).templateId == selId) {
				Log.v(LOG_TAG, "True: "+ events.get(i).name);
				events.get(i).box = true;
			} else {
				Log.v(LOG_TAG, "False: "+ events.get(i).name);
				events.get(i).box = false;
			}
			//Log.v(LOG_TAG, "After: "+ Boolean.toString(events.get(i).box));
		}
	}
}
