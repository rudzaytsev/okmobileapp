package ru.spbstu.icc.kspt.eventsearcher;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TemplatesActivity extends Activity {

  private static final int CM_DELETE_ID = 1;
   
  public static final String LOG_TAG = "TemplatesActivity";
  ListView lvTemplates;
  Long selectedTemplateId;
  
  ArrayAdapter<String> adapter;
  TemplateInfo templateInfo = new TemplateInfo();
  ArrayList<String> templates = new ArrayList<String>();  
  
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.templates);
    Intent intent=getIntent();
    selectedTemplateId = intent.getLongExtra("selTid", 1);

    // ���������� ������ � ����������� ��� �������
    lvTemplates = (ListView) findViewById(R.id.lvTemplates);
    
    final EditText editText = (EditText) findViewById(R.id.editText); 
    
    
    DbProcessor dbp = new DbProcessor(new DbConnector(getBaseContext()));
    dbp.openDatabase();
    FillData.fillTemplateList(dbp.getTemplate(null), templateInfo);
    dbp.closeDatabase();
    
    templates = templateInfo.toArrayList();
    adapter = new ArrayAdapter<String>(this,
            android.R.layout.simple_list_item_single_choice, templates);
    
    lvTemplates.setAdapter(adapter);
    lvTemplates.setItemChecked(templateInfo.getPosById(selectedTemplateId), true);
    
    // ��� ������� �� ������ ������� ������ ����� �������
    lvTemplates.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    	@Override
    	public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
    		TextView textView = (TextView) itemClicked;
    		Toast.makeText(getApplicationContext(), textView.getText(),
    		        Toast.LENGTH_LONG).show();
    		selectedTemplateId = templateInfo.getIdByPos(lvTemplates.getCheckedItemPosition());
    	}
    });
    
    editText.setOnKeyListener(new OnKeyListener() {
		public boolean onKey(View v, int keyCode, KeyEvent event) {

			if (event.getAction() == KeyEvent.ACTION_DOWN)
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					templates.add(editText.getText().toString());
					templateInfo.add(new TemplateElem(null,editText.getText().toString()));
					adapter.notifyDataSetChanged();
					editText.setText("");
					return true;
				}
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				Intent intent = new Intent();
				intent.putExtra("selectedTemplateId", selectedTemplateId);
			    setResult(RESULT_OK, intent);
			    Log.w(LOG_TAG, "Nya!?");
			    finish();
			}
			return false;
		}
	});
    
   
          
    registerForContextMenu(lvTemplates);
  }
  
  
  /*private class ShowTemplates extends AsyncTask<Context, Void, String> {
		private Context context;
      @Override
      protected String doInBackground(Context... params) {
      	context = params[0];
      	DbProcessor dbp = new DbProcessor(new DbConnector(context));
		ArrayList<String> id = null;
		FillData.fillTemplateList(dbp.getTemplate(""), templates, id);
  		dbp.close();
          return null;
      }
      @Override
      protected void onPostExecute(String result) {

  		adapter = new ArrayAdapter<String>(context,
  	            android.R.layout.simple_list_item_single_choice, templates);
		
  		ListView lvTemplates = (ListView) findViewById(R.id.lvTemplates);
  	    lvTemplates.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
  	    lvTemplates.setAdapter(adapter);
  	  registerForContextMenu(lvTemplates);
      }
	}*/
  

  //��� ������� ������� ������� ������
  @Override
  public void onCreateContextMenu(ContextMenu menu, View v,
      ContextMenuInfo menuInfo) {
    super.onCreateContextMenu(menu, v, menuInfo);
    menu.add(0, CM_DELETE_ID, 0, "������� ������");
  }

  public void onClick_SaveTemplate(View v) {
	Log.v(LOG_TAG, "onClick_SaveTemplate");
    DbProcessor dbp = new DbProcessor(new DbConnector(getBaseContext()));
    dbp.openDatabase();
    dbp.updateTemplatesFromList(templateInfo);
    dbp.closeDatabase();

    
    Intent intent = new Intent();
	intent.putExtra("selectedTemplateId", selectedTemplateId);
    setResult(RESULT_OK, intent);
    Log.w(LOG_TAG, "Nya!?");
    finish();
  }
  
  @Override
  public boolean onContextItemSelected(MenuItem item) {
    if (item.getItemId() == CM_DELETE_ID) {
      AdapterContextMenuInfo acmi = (AdapterContextMenuInfo) item.getMenuInfo();// �������� ���� � ������ ������
      templates.remove(acmi.position);// ������� Map �� ���������, ��������� ������� ������ � ������
      templateInfo.deleteAt(acmi.position);
      adapter.notifyDataSetChanged(); // ����������, ��� ������ ����������
      return true;
    }
    return super.onContextItemSelected(item);
  }
}