package ru.spbstu.icc.kspt.eventsearcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class FillData {
	static DateTimeFormatter birthdayFull = DateTimeFormat.forPattern("yyyy-MM-dd");
	static DateTimeFormatter birthdayShort = DateTimeFormat.forPattern("MM-dd");
	
	
	public static final String LOG_TAG = "FillData";
		public static void fillFriendList(Cursor cur, ArrayList<InfoElem> list, ArrayList<TaskInfo> taskInfo){
		Bitmap bitmap;
		byte [] img;
		TaskInfo info;
		if(cur.moveToFirst()){
			int indFirstName = cur.getColumnIndex("first_name");
        	int indLastName = cur.getColumnIndex("last_name");
        	int indBirthday = cur.getColumnIndex("birthday");
        	int indImage = cur.getColumnIndex("avatar_image");
        	int indSid = cur.getColumnIndex("sid");
        	int indUid = cur.getColumnIndex("uid");
        	int indParams = cur.getColumnIndex("params");
        	int indTime = cur.getColumnIndex("time");
        	Long sid = -1L, tid = -1L, time = 0L;
        	String birthday;
		    do{
		    	Log.v(LOG_TAG, " first_name = " + cur.getString(indFirstName) + 
        				" last_name = " + cur.getString(indLastName) + 
        				" birthday = " + cur.getString(indBirthday));
		    	img = cur.getBlob(indImage);
		    	bitmap = BitmapFactory.decodeByteArray(img, 0, img.length);
		    	
		    	if(indSid >=0){
		    		try {
		    			sid = cur.getLong(indSid);
		    			tid = Long.parseLong(cur.getString(indParams));
		    			time = cur.getLong(indTime);
		    		} catch (Exception e){
		    			//e.printStackTrace();
		    			sid = -1L;
		    			tid = -1L;
		    			birthday = cur.getString(indBirthday);
		    			Log.v(LOG_TAG, birthday +" =  " + Integer.toString(birthday.length()));
		    			if(birthday.length()==10) {
		    				time = DateTime.parse(cur.getString(indBirthday), birthdayFull).getMillis();
		    			} else {
		    				time = DateTime.parse(cur.getString(indBirthday), birthdayShort).getMillis();
		    			}
		    			
		    		}
		    	} 
		    	if(taskInfo!=null) {
		    		info = new TaskInfo(sid, cur.getLong(indUid), tid, time);
		    		taskInfo.add(info);
		    	}
		    	list.add(new InfoElem(cur.getString(indLastName) + "  " +
		    									cur.getString(indFirstName),
		    									cur.getString(indBirthday),
		    									bitmap, sid>=0));
		    }while(cur.moveToNext());
		}else{
		    Log.w(LOG_TAG, "table friends empty");
		}
	}
	
	public static String fillMessageTemplate(Cursor cur, String template){
		if(cur.moveToFirst()){
			int indFirstName = cur.getColumnIndex("first_name");
	    	int indLastName = cur.getColumnIndex("last_name");
	    	template = template.replace("$first_name", cur.getString(indFirstName));
	    	template = template.replace("$last_name", cur.getString(indLastName));
	    	Log.w(LOG_TAG, template);
		}else{
		    Log.w(LOG_TAG, "table friends  empty");
		}
		return template;
	}
	
	public static void fillTaskList(Cursor cur, HashMap<Long, DateTime> activeTasks, LinkedList<Intent> list, Intent intent){
		if(cur.moveToFirst()){
			int indId = cur.getColumnIndex("sid");
        	int indTaskId = cur.getColumnIndex("task_id");
        	int indUserId = cur.getColumnIndex("user_id");
        	int indParams = cur.getColumnIndex("params");
        	int indTimeout = cur.getColumnIndex("timeout");
        	int taskId;
        	DateTime timeout;
        	Intent tmpIntent;
		    do{
		    	taskId = cur.getInt(indTaskId);
		    	timeout = activeTasks.get(taskId);
		    	if(timeout == null || timeout.isBeforeNow()){
		    		tmpIntent = new Intent(intent);
			    	tmpIntent.putExtra("cmd", MainService.Command.values()[taskId]);
			    	tmpIntent.putExtra("params", cur.getString(indParams));
			    	tmpIntent.putExtra("sid", cur.getLong(indId));
			    	tmpIntent.putExtra("uid", cur.getLong(indUserId));
			    	list.push(tmpIntent);
			    	activeTasks.put(cur.getLong(indId), DateTime.now().plusMillis(cur.getInt(indTimeout)));
		    	}
		    }while(cur.moveToNext());
		}else{
		    Log.v(LOG_TAG, "table schedule empty");
		}
	}
	
	public static void fillAvatar(Cursor cur, Map<Long, String> arr){
		if(cur.moveToFirst()){
			int indId = cur.getColumnIndex("uid");
        	int indLink = cur.getColumnIndex("avatar_link");
			do{
				arr.put(cur.getLong(indId), cur.getString(indLink));
			} while(cur.moveToNext());
		}
	}

	public static void fillTemplateList(Cursor cur, TemplateInfo templates){
		if(cur.moveToFirst()){
			int indId = cur.getColumnIndex("tid");
        	int indTemplate = cur.getColumnIndex("template");
			do{
				templates.add(new TemplateElem(cur.getLong(indId), cur.getString(indTemplate)));
			} while(cur.moveToNext());
		}
	}
	
	/*public static void fillTemplateList(Cursor cur, ArrayList<String> templates, ArrayList<Long> templateId){
		if(cur.moveToFirst()){
			int indId = cur.getColumnIndex("tid");
        	int indTemplate = cur.getColumnIndex("template");
			do{
				templates.add(cur.getString(indTemplate));
				if(templateId!=null) templateId.add(cur.getLong(indId));
			} while(cur.moveToNext());
		}
	}*/

}
