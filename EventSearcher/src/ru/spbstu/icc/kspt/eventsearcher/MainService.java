package ru.spbstu.icc.kspt.eventsearcher;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
/*
 * ������, � �������� ���������� ������������ Activity � ����-������������ (��������: ������ 30 ����� ��������� ��� �������� ������)
 * �������������� ����� ����������� �������� ����������� �� �� ������ � ��� �� ���������.
 */
public class MainService extends Service {
    public static final String LOG_TAG = "MainService";
    public static enum Command {
    	LOGIN, UPDATE_OK_LOGIN, UPDATE_FRIENDSINFO, LIST_FRIENDS, LIST_TEMPLATES, SEND_MESSAGE, SEND_MESSAGE_TEMPLATE, TEST_TASK
    }
    private OkProcessor mOdnoklassniki;
    private Scheduler mScheduler;
    private DbProcessor dbp;

    @Override
	public void onCreate() {
    	super.onCreate();
    	dbp = new DbProcessor(new DbConnector(this));
    	mOdnoklassniki = new OkProcessor(getBaseContext());
    	//Toast.makeText(this, "MainService: Start", Toast.LENGTH_LONG).show();
    	mScheduler = new Scheduler(getBaseContext());
    	mScheduler.start();
    	Log.v(LOG_TAG, "MainService: Start");
    }
    
    public int onStartCommand(Intent intent, int flags, int startId) {
    	MainService.Command cmd = (MainService.Command)intent.getSerializableExtra("cmd");
        Log.v(LOG_TAG, "onStartCommand");
        if(cmd != null){
	        switch (cmd){
	        	case LOGIN:
	        		intent = new Intent(getBaseContext(), LoginActivity.class);
        	        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);        	      
        	        startActivity(intent);
        	        break;
	        	case UPDATE_FRIENDSINFO:
	        		mOdnoklassniki.getFriendsInfo();
	        		break;
	        	case LIST_FRIENDS:
        	        intent = new Intent(getBaseContext(), FriendsActivity.class);
        	        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        	        startActivity(intent);
	        		break;
	        	/*case LIST_TEMPLATES:
	        		Long sel = intent.getLongExtra("sel", 1);
	        		intent = new Intent(getBaseContext(), TemplatesActivity.class);
        	        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        	        intent.putExtra("sel", sel);
        	        startActivity(intent);
	        		break;*/
	        	case SEND_MESSAGE:
	        		mOdnoklassniki.sendMessage(Long.toString(intent.getLongExtra("uid", 0)), intent.getStringExtra("params"));
	        		break;
	        	case SEND_MESSAGE_TEMPLATE:
	        		Log.v(LOG_TAG, "zz");
	        		dbp.openDatabase();
	        		String message = dbp.getMessageTemplate(Long.parseLong(intent.getStringExtra("params")));
	        		message = FillData.fillMessageTemplate(dbp.getFriendsInfo(intent.getLongExtra("uid", -1)),message);
	        		dbp.closeDatabase();
	        		mOdnoklassniki.sendMessage(Long.toString(intent.getLongExtra("uid", 0)), message);
	        		break;
	        	case TEST_TASK:
	        		Log.v(LOG_TAG, "TEST_TASK");
	        		break;
	        	default:
	        		break;
	        }
	        Long sid = intent.getLongExtra("sid", -1);
	        //���� ���, � ������ ����� ���������� ������������ � �� ���� ������������.
	        if(sid>0){
	        	Log.v(LOG_TAG, "SID : "+sid.toString());
	        	mScheduler.finishTask(sid);
	        }
        }
        return super.onStartCommand(intent, flags, startId);
    }
    
    @Override
	public void onDestroy() {
    	//Toast.makeText(this, "MainService: Destroy", Toast.LENGTH_LONG).show();
    	mScheduler.interrupt();
    	mOdnoklassniki.disconnet();
    	Log.v(LOG_TAG, "MainService: Destroy");
    	super.onDestroy();
    }
    
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return null;
    }
}