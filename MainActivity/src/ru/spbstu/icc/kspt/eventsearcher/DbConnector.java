package ru.spbstu.icc.kspt.eventsearcher;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbConnector extends SQLiteOpenHelper {
	public static final String LOG_TAG = "DbConnector";

	private static final int DB_VERSION = 1;
	private static final String DB_NAME = "event_searcher_db";

	private static final String CREATE_TABLE 
		= "create table friends ( uid integer primary key" +
									", first_name text" +
									", last_name text" +
									", birthday text" +
									"); "+
		"create table schedule ( sid integer primary key autoincrement" +
									", time text" +
									", period integer" +
									", task_id integer" +
									", params" +
									"); ";

	public DbConnector(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase) {
		sqLiteDatabase.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
		String str = "drop table friends; drop table schedule;";
		sqLiteDatabase.execSQL(str);
		this.onCreate(sqLiteDatabase);
	}
}