package ru.spbstu.icc.kspt.eventsearcher;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkTokenRequestListener;
import ru.ok.android.sdk.util.OkScope;

/*
 * Based on OK samples (which was created by valery.ozhiganov on 26.12.13).
 */
public class
        LoginActivity extends Activity implements OkTokenRequestListener, View.OnClickListener {

    private Odnoklassniki mOdnoklassniki;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

    //create object which will bound to the application context
        mOdnoklassniki = Odnoklassniki.createInstance(getApplicationContext(),
        												ConstantsApiOk.APP_ID,
        												ConstantsApiOk.APP_SECRET_KEY,
        												ConstantsApiOk.APP_PUBLIC_KEY);

    //define callback on authorization
        mOdnoklassniki.setTokenRequestListener(this);
    }

    @Override
    public void onDestroy() {
    //remove callback
        mOdnoklassniki.removeTokenRequestListener();
        super.onDestroy();
    }

    //if all right, user logged in our app
    @Override
    public void onSuccess(String token) {
        Log.v("APIOK", "Your token: " + token);
        Intent intent = new Intent(this, MainService.class);
    	intent.putExtra("cmd", 2);
    	startService(intent);
        finish();
    }

    //if something goes wrong in authorization
    @Override
    public void onError() {
        Log.v("APIOK", "Error");
        Toast.makeText(this, "Something goes wrong", Toast.LENGTH_LONG).show();
    }

    //if something goes wrong in authorization
    @Override
    public void onCancel() {
        //press back
        //press cancel
        Log.v("APIOK", "Cancel");
        Toast.makeText(this, "Something goes wrong", Toast.LENGTH_LONG).show();
    }

    //processing user click
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginButton:
                Log.v("APIOK", "Clicked");
                //request authorization
                mOdnoklassniki.requestAuthorization(this, false, OkScope.VALUABLE_ACCESS);

                break;
        }
    }
}
