package ru.spbstu.icc.kspt.eventsearcher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ru.ok.android.sdk.Odnoklassniki;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

/*
 * ����� ������� ��� ������� � API, ������ ������� ����������� ��� AsyncTask
 * ������� ����� �� "��������" ��-�� ������� ��������
 */
class OkProcessor {
    public static final String LOG_TAG = "OkProcessor";
    private Odnoklassniki mOdnoklassniki;
    private Context mContext;
    
    public OkProcessor(Context context) {
    	mContext = context;
    }
    
    public void login() {
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("cmd",1);
        mContext.startActivity(intent);
    }
    
    public void update(Odnoklassniki obj) {
        mOdnoklassniki = obj;
        Log.v(LOG_TAG, mOdnoklassniki.getCurrentAccessToken());
    }
    
    public void getBirthDays() {
        // ��������� ����������� ���������� �������
        new LoadFriends().execute();
    }
      
    private class LoadFriends extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                return mOdnoklassniki.request("friends.get", null, "get");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    
        @Override
        protected void onPostExecute(String result) {
            try {
                Log.v("APIOK", "Response on friends.get: " + result);
                // ��-�� ����������� API �� �� ����� ����� ��������� ���������� ����� ��� � 100 �������������,
                // � ������ ����� ���� �� 10000
                ArrayList<String> friendsId = jsonArrayToStringArray(result, ConstantsApiOk.UsersGetInfoMaxUids);
                for (int i = 0; i < friendsId.size(); i++) {
                    new LoadFriendsInfo().execute(friendsId.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    
        // ����������� ���������� �� ������� ������� � ������ �� ����� �������������
        private ArrayList<String> jsonArrayToStringArray(String str, int maxElemPerString) throws JSONException {
            ArrayList<String> result = new ArrayList<String>();
            JSONArray array = new JSONArray(str);
            StringBuilder builder = new StringBuilder();
            for (int iInStr = 0, iInArr = 0; iInArr < array.length(); iInArr++){
                if (iInStr == maxElemPerString) {
                    result.add(builder.substring(1));
                    builder = new StringBuilder();
                    iInStr = 0;
                }
                String element = array.getString(iInArr);
                builder.append(',').append(element);
                iInStr++;
            }
            result.add(builder.substring(1));
            return result;
        }
    }
    
    //class for method users.getInfo
    //api methods must called from not UI thread
    // ����� ������� �������������
    private class LoadFriendsInfo extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String friendsId = params[0];
    
            //put in Map GET-parameters
            //in our case uids - friend's id, fields - requested fields
            Map<String, String> requestParams = new HashMap<String, String>();
            requestParams.put("uids", friendsId);
            requestParams.put("fields", "last_name, first_name, birthday");
    
            try {
                //calling api method
                return mOdnoklassniki.request("users.getInfo", requestParams, "get");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    
        @Override
        protected void onPostExecute(String result) {
            Log.v("APIOK", "Response on users.getInfo: " + result);
            ContentValues cv = new ContentValues();
            DbConnector dbc = new DbConnector(mContext);
            SQLiteDatabase db = dbc.getWritableDatabase();
            try {
				JSONArray array = new JSONArray(result);
				JSONObject obj;
				for (int i = 0; i < array.length(); i++) {
					obj = array.getJSONObject(i);
					cv.put("uid", obj.getString("uid"));
		        	cv.put("first_name", obj.getString("first_name"));
		        	cv.put("last_name", obj.getString("last_name"));
		        	// ���� ���� �� ��������������� - ��� ��������
		        	cv.put("birthday", obj.getString("birthday"));
		        	db.insert("friends", null, cv);
				}
				db.close();
			} catch (Exception e) {
				db.close();
				e.printStackTrace();
			}
        }
    }
}