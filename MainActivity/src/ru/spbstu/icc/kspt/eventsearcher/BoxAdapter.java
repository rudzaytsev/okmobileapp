package ru.spbstu.icc.kspt.eventsearcher;
import java.util.ArrayList;

import ru.spbstu.icc.kspt.eventsearcher.list_class;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
public class BoxAdapter extends BaseAdapter {
Context ctx;
LayoutInflater lInflater;
ArrayList<list_class> objects;
BoxAdapter(Context context, ArrayList<list_class> list_object) {
	ctx = context;
	objects = list_object;
	lInflater = (LayoutInflater) ctx
	.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
}
// ���-�� ���������
@Override
public int getCount() {
	return objects.size();
}
// ������� �� �������
@Override
public Object getItem(int position) {
	return objects.get(position);
}
// id �� �������
@Override
public long getItemId(int position) {
	return position;
}
// ����� ������
@Override
public View getView(int position, View convertView, ViewGroup parent) {
	// ���������� ���������, �� �� ������������ view
	View view = convertView;
	if (view == null) {
		view = lInflater.inflate(R.layout.item, parent, false);
	}
	list_class p = getlist_class(position);
	// ��������� ������: ���, ���� � ��������
	((ImageView) view.findViewById(R.id.ivImage)).setImageResource(p.image);
	((TextView) view.findViewById(R.id.tvDescr)).setText(p.name);
	((TextView) view.findViewById(R.id.tvDate)).setText(p.date + "");
	CheckBox cbBuy = (CheckBox) view.findViewById(R.id.cbBox);
	// ����������� �������� ����������
	cbBuy.setOnCheckedChangeListener(myCheckChangList);
	// ����� �������
	cbBuy.setTag(position);
	// ��������� �������: ������� ��� ���
	cbBuy.setChecked(p.box);
	return view;
}

// ����/������� �� �������
list_class getlist_class(int position) {
	return ((list_class) getItem(position));
}

// ���������
ArrayList<list_class> getBox() {
	ArrayList<list_class> box = new ArrayList<list_class>();
	for (list_class p : objects) {
		// ���� �������:
		if (p.box)
			box.add(p);
	}
	return box;
}

// ���������� ��� ���������
OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// ������ ����� 
		getlist_class((Integer) buttonView.getTag()).box = isChecked;
		}
	};
}
