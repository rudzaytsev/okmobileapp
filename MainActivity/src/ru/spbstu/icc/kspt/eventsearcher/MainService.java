package ru.spbstu.icc.kspt.eventsearcher;

import ru.ok.android.sdk.Odnoklassniki;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
/*
 * ������, � �������� ���������� ������������ Activity � ����-������������ (��������: ������ 30 ����� ��������� ��� �������� ������)
 * �������������� ����� ����������� �������� ����������� �� �� ������ � ��� �� ���������.
 */
public class MainService extends Service {
    public static final String LOG_TAG = "MainService";
    public enum cmd {
    	LOGIN, UPDATE_OK_LOGIN, UPDATE_BIRTHSDAYS
    }
    private OkProcessor mOdnoklassniki;
//    private DbConnector dbc;

    @Override
	public void onCreate() {
    	super.onCreate();
    	mOdnoklassniki = new OkProcessor(getBaseContext());
    	/*
    	DbConnector dbc = new DbConnector(getBaseContext());
        SQLiteDatabase db = dbc.getWritableDatabase();
        dbc.onUpgrade(db, 1, 1);
        */
    	Toast.makeText(this, "MainService: Start", Toast.LENGTH_LONG).show();
    	Log.v(LOG_TAG, "MainService: Start");
    }
    
    public int onStartCommand(Intent intent, int flags, int startId) {
    	int cmd = intent.getIntExtra("cmd", 0);
        Log.v(LOG_TAG, "onStartCommand");
        switch (cmd){
        	case 1:
        	    mOdnoklassniki.login();
        	    break;
        	case 2: // ����� "�����" ��������� private static Odnoklassniki sOdnoklassniki; � SDK
        		mOdnoklassniki.update(Odnoklassniki.getInstance(getBaseContext()));
        		break;
        	case 3: //������ �� ��������� ����� - �������� ��� ��� �������� ���� ������
        		mOdnoklassniki.getBirthDays();
        		break;
        	default:
        		break;
        	     
        }
        return super.onStartCommand(intent, flags, startId);
    }
    
    @Override
	public void onDestroy() {
    	Toast.makeText(this, "MainService: Destroy", Toast.LENGTH_LONG).show();
    	Log.v(LOG_TAG, "MainService: Destroy");
    	super.onDestroy();
    }
    
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(LOG_TAG, "onBind");
        return null;
    }

}