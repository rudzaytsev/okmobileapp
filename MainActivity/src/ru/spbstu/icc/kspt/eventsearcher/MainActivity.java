package ru.spbstu.icc.kspt.eventsearcher;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;



public class MainActivity extends Activity {
	ArrayList<list_class> list_object = new ArrayList<list_class>();
	ArrayList<list_class> list_object1 = new ArrayList<list_class>();
	BoxAdapter boxAdapter;
	BoxAdapter boxAdapter1;
	/** Called when the activity is first created. */
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		TabHost tabHost = (TabHost)findViewById(R.id.tabhost);
        
        tabHost.setup();
        
        TabHost.TabSpec friend_tab = tabHost.newTabSpec("������");
        friend_tab.setIndicator("������");
        friend_tab.setContent(R.id.friends_layout);
        
        TabHost.TabSpec events_tab = tabHost.newTabSpec("�������");
        events_tab.setIndicator("�������");
        events_tab.setContent(R.id.events_layout);
        
        tabHost.addTab(friend_tab);
        tabHost.addTab(events_tab);
        
        tabHost.setCurrentTab(0);
        
      
        
		// ������� �������
		fillData();
		
		boxAdapter = new BoxAdapter(this, list_object);
		// ����������� ������
		ListView lvMain_us = (ListView) findViewById(R.id.user_list);
		lvMain_us.setAdapter(boxAdapter);
		
		boxAdapter1 = new BoxAdapter(this, list_object1);
		ListView lvMain_ev = (ListView) findViewById(R.id.events_list);
		lvMain_ev.setAdapter(boxAdapter1);
	}
	// ���������� ������ ��� ��������
	void fillData() {
		for (int i = 1; i <= 20; i++) {
			
			list_object.add(new list_class("������� ��� ������� " + i, i+"."+i+"."+(1950+i),
			R.drawable.avatar, false));
			
			list_object1.add(new list_class("�������� ������� " + i, i+"."+i+"."+(1950+i),
			R.drawable.ic_launcher, false));
		}
	}
	
	
	// ������� ���������� � ��������� �������
	public void showResult(View v) {
		String result = "�������:";
		for (list_class p : boxAdapter.getBox()) {
			if (p.box)
				result += "\n" + p.name;
			}
		Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
	}
	public void showResult1(View v) {
		String result = "�������:";
		for (list_class q : boxAdapter1.getBox()) {
			if (q.box)
				result += "\n" + q.name;
			}
		Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
	}

	
	public void onClickSynchr(View v) {
		showResult(v);
	}
	
	public void onClickSynchr1(View v) {
		showResult1(v);
	}


/*
 * Activity ��� ������� ��������� � �������

public class MainActivity extends Activity implements View.OnClickListener {
	


   /*@Override
    public void onClick(View view) {
    	
    }*/
    
   /* public void onClickStart(View view) {
       	startService(new Intent(this, MainService.class));
    }
    
    public void onClickStop(View view) {
    	stopService(new Intent(this, MainService.class));
    }
    
    public void onClickAction(View view) {
    	Intent intent = new Intent(this, MainService.class);
    	intent.putExtra("cmd", 3);
    	startService(intent);
    }
    
    public void onClickLogin(View view) {
    	Intent intent = new Intent(this, MainService.class);
    	intent.putExtra("cmd", 1);
    	startService(intent);
    }
    */
    @Override
    public void onDestroy() {
    	super.onDestroy();
    }
}